This fork of OAI can be used for non-intrusive execution times. The changes made record all timing measurements into pre-allocated memory, and write them to files, after a given time period. (Instead of periodically printing average, standard deviation and maximum values.) This makes for example generating histogram plots and other finer inspection of execution times possible.

The fork also reads SDR devices' IP addresses from environment variables. This allows running of multiple OAI processes with multiple SDRs simultaneously.

### How to use the fork

1. Determine the number of samples you want to gather and set it as the 'MEAS_BUFF_SIZE' in 'time_meas.h'.
2. Set the return value of 'get_cpu_freq_GHz()' in 'time_meas.c' as the fixed frequency of your CPU.
2. Change the path that is passed to the 'snprintf()' in 'time_meas.c' to suit your needs.
3. If you want to use multiple SDRs, find out their IP addresses. They most likely use the IP address that it next of the corresponding interface's IP address. (If the interface has the IP 192.168.30.1, the device most likely has 192.168.30.2.) This can be passed to OAI as the environment variable 'X310ADDR'.
4. The environment variable 'TESTNAME' has to be set. That will affect the path of the directory, to which the measurements are written to.
5. Change the 'sleep()' times before the calls of the 'write_meas()' in 'lte-ru.c' and 'lte-enb.c' to suit your needs. This is the amount of time that will be slept until the test results are written into the corresponding files. The timeout of the tests should be set some 30 seconds larger, so that the initiation time of the OAI is taken into consideration. (So that the processes are not killed before the sleeps finish.)
6. The 'run-tests.sh' can be used as an example of automating multiple tests.
7. Parsing of the test results can be done with the provided 'parse.py' and plotting of histograms can be done with the provided 'hist.py'.

*I, Eino Virtanen, will leave Eurecom on the 31st of July, 2018. I can provide guidance for measurements through email: eino.virtanen@alumni.aalto.fi . But as a disclaimer, I will probably start forgetting about the practicalities of the testing after a few weeks of my departure. So don't hesitate to contact me ASAP, so that I can provide as fresh guidance as possible.*

