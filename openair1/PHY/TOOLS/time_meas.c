/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.1  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

#include <stdio.h>
#include "time_meas.h"
#include <math.h>
#include <unistd.h>

// global var for openair performance profiler
int opp_enabled = 0;

double get_cpu_freq_GHz() {
  return (double)3.0;
}

void print_meas_now(time_stats_t *ts, const char* name, FILE* file_name){
  ;
}

void write_meas(time_stats_t *ts, const char* name, const char* testname)
{
  if (ts->started) {
    int i;
    char float_buffer[10];
    FILE * fpointer;
    char full_path[200];
    snprintf(full_path, sizeof(full_path), "/home/virtanen/test_results/%s/%s-%p.txt", testname, name, ts);
    fprintf(stderr, "attempting to write to %s\n", full_path);
    fpointer = fopen(full_path, "w");
    for (i = 0; i < ts->trials; i++) {
      snprintf(float_buffer, sizeof(float_buffer), "%0.6f;", ts->measurements[i]);
      fputs(float_buffer, fpointer);
    }
    fclose(fpointer);
  }
}


void print_meas(time_stats_t *ts, const char* name, time_stats_t * total_exec_time, time_stats_t * sf_exec_time) {
  ;
}

double get_time_meas_us(time_stats_t *ts){return 0;} // used only in deprecated simulations

