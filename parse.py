from string import replace, count
from numpy import mean, std
from sys import argv
from os import listdir
tlen = 380
lsplit = 40.0/(tlen)
hsplit = 1.0*(tlen-40)/(tlen)
argv[1] = argv[1] + "/"
if len(argv) != 2:
  print "Usage: python2 parse.py [test path]"
else:
  first_print = True
  value_path_list = [argv[1]+value for value in listdir(argv[1])]
  measurement_path_list = []
  for value_path in value_path_list:
    test_paths = listdir(value_path)
    for test_path in test_paths:
      measurement_paths = listdir(value_path+"/"+test_path)
      for measurement_path in measurement_paths:
        measurement_path_list.append(value_path+"/"+test_path+"/"+measurement_path)
  for measurement_path in measurement_path_list:
    file_obj = open(measurement_path, "r")
    measurement_data = file_obj.read()
    if first_print:
      print("parameter;value;test n:o;measurement;average;standard deviation;max")
      first_print = False
    meas_str = measurement_path.split("/")
    if "stderr" in measurement_path:
      print meas_str[-4]+";"+meas_str[-3]+";"+meas_str[-2]+";late_transmissions;"+str(count(measurement_data, "L")-1) # _L_OADER print
      print meas_str[-4]+";"+meas_str[-3]+";"+meas_str[-2]+";SDR_underflows;"+str(count(measurement_data, "U"))
    else:
      fields = measurement_data.split(";")
      values = []
      for field in fields:
        try:
          values.append(float(field))
        except:
          pass
      len_values = len(values)
      values = values[int(len_values*lsplit):int(len_values*hsplit)]
      if len(values) > 0:
        print meas_str[-4]+";"+meas_str[-3]+";"+meas_str[-2]+";"+replace(meas_str[-1],".txt","")[:-15]+";"+str(mean(values))+";"+str(std(values))+";"+str(max(values))
    file_obj.close()

