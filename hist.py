#!/usr/bin/env python

# yhessa kestaa yks minuutti

import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from os import listdir
from math import sqrt, ceil, floor
from time import strftime
print("Plot generation started at:", strftime("%H:%M"))
testtitle = 'Default scheduling values, mcs 28'
testdir = 'E:\\testidata\\test_results\\mcs28\\4000000'
mcs = listdir(testdir)
data = {}
for mc in mcs:
    measurements = listdir(testdir + '\\' + mc)
    for measurement in measurements:
        if 'stdin' not in measurement:
            if 'stderr' not in measurement:
                if 'signal' not in measurement:
                    if 'wait' not in measurement:
                        fileobj = open(testdir + '\\' + mc + '\\' + measurement)
                        shortmeasurement = measurement[:-19]
                        if len(shortmeasurement) > 0:
                            testdata = fileobj.read().split(';')
                            data[shortmeasurement] = []
                            for datapoint in testdata:
                                try:
                                    data[shortmeasurement].append(float(datapoint)*1000)
                                except:
                                    pass
                        fileobj.close()

gw = ceil(sqrt(len(data))) # grid width
plt.rcParams.update({'font.size': 4})
for meastype in data:
    if "ofdm_demod" in meastype:
        meastitle = "1 OFDMA demodulation"
        gi = 1
    elif "ulsch_decoding_stats" in meastype:
        meastitle = "2 ULSCH decoding"
        gi = 2
    elif "dlsch_encoding_stats" in meastype:
        meastitle = "3 DLSCH encoding total"
        gi = 3
    elif "dlsch_turbo_encoding_preperation" in meastype:
        meastitle = "3.1 DLSCH turbo encoding preparation"
        gi = 4
    elif "dlsch_turbo_encoding_segmentation" in meastype:
        meastitle = "3.2 DLSCH turbo encoding segmentation"
        gi = 5
    elif "dlsch_turbo_encoding_main_stats" in meastype:
        meastitle = "3.3 DLSCH turbo encoding"
        gi = 6
    elif "dlsch_modulation_stats" in meastype:
        meastitle = "4. DLSCH modulation"
        gi = 7
    elif "ofdm_mod_stats" in meastype:
        meastitle = "5. OFDMA modulation"
        gi = 8
    plt.subplot(gw, gw, gi)
    n, bins, patches = plt.hist(np.array(data[meastype]), 'auto', facecolor='blue', alpha=0.75, log=True)
    plt.xlabel('Execution time [µs]')
    plt.ylabel('Number of exections [n]')
    plt.title(meastitle)
    #plt.autoscale(axis='y')
    
    plt.autoscale(tight=True)

    #ti: autoscale pois kaytosta x-akselilla ja koita saada siihe custom rajat
    
    xlocslist = [min(data[meastype])]
    xlabelslist = ['%.2f' % min(data[meastype])]
    for xticka in list(plt.xticks()[0]):
        xlocslist.append(xticka)
        xlabelslist.append(str(xticka).rstrip('0').rstrip('.'))
    xlocslist.pop()
    xlocslist.pop()
    xlocslist.pop(1)
    xlocslist.pop(1)
    xlabelslist.pop()
    xlabelslist.pop()
    xlabelslist.pop(1)
    xlabelslist.pop(1)
    xlocslist.append(max(data[meastype]))
    xlabelslist.append('%.2f' % max(data[meastype]))
    
    plt.xticks(xlocslist, xlabelslist)
    
    #plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #plt.grid(True)
plt.tight_layout()
plt.savefig("".join(x for x in testtitle if x.isalnum()), dpi=600)
print("Plot generation done at:", strftime("%H:%M"))


