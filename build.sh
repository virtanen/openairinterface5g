#!/bin/bash

#if [[ $EUID -eq 0 ]]; then
#   echo "Running this as root is probably unwise"
#   exit 1
#fi

source ./oaienv
SKIP_BROKEN=--skip-broken cmake_targets/build_oai --hardware USRP --eNB -C # -I --vcd -t ETHERNET
