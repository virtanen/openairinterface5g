#!/bin/bash

NAME_OF_PARAMETER="sched_wakeup_granularity_ns"
PARAMETER_VALUE=4000000
TEST_DURATION=60
SLEEP_DURATION=60

if [[ $EUID -ne 0 ]]; then
   echo "Running this as root is probably wiser"
   exit 1
fi

clear

#for CORE in `seq 16 35`
#do
#  echo 1 > /sys/devices/system/cpu/cpu${CORE}/online
#done

#while :
#do
  #sysctl kernel.${NAME_OF_PARAMETER}=$PARAMETER_VALUE
  mkdir -p /home/virtanen/test_results/${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1
  bash -c "X310ADDR=192.168.30.2 TESTNAME=${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1 timeout ${TEST_DURATION}s cmake_targets/lte_build_oai/build/lte-softmodem -q -O enb.band7.tm1.100PRB.double.usrpx310.2155.conf --codingw --fepw --phy-test 2>/home/virtanen/test_results/${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1/stderr1.txt >/home/virtanen/test_results/${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1/stdout1.txt" &
  bash -c "X310ADDR=192.168.50.2 TESTNAME=${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1 timeout ${TEST_DURATION}s cmake_targets/lte_build_oai/build/lte-softmodem -q -O enb.band7.tm1.100PRB.double.usrpx310.2165.conf --codingw --fepw --phy-test 2>/home/virtanen/test_results/${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1/stderr2.txt >/home/virtanen/test_results/${NAME_OF_PARAMETER}/${PARAMETER_VALUE}/1/stdout2.txt" &
  #sleep $SLEEP_DURATION
  #if [[ $PARAMETER_VALUE -eq 0 ]]; then
  #  exit 1
  #fi
  #let "PARAMETER_VALUE = PARAMETER_VALUE / 10 * 9" # -10%
#done
